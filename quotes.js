const quotes = [
	{
		text: '&quot;'+'A morte não é a maior perda da vida. A maior perda da vida é o que morre dentro de nós enquanto vivemos...'+'&quot;',
		source: "Pablo Picasso",
	},
	{
		text: '&quot;'+ 'Não há beleza rara sem algo de estranho nas proporções.' +'&quot;',
		source: 'Edgar Allan Poe, Ligeia',
	},
	{
		text: '&quot;'+ 'Em nossa sociedade, os lugares interessantes, na sua maioria, ou são ilegais ou muito caros.' +'&quot;',
		source: 'Charles Bukowski',
	},
	{
		text: '&quot;'+ 'Baby Shark tutururututu, Baby Shark tutururututu, Baby Shark tutururututu, Baby Shark.' +'&quot;',
		source: 'Aristóteles',
	},
	{
		text: '&quot;'+ 'Tentei fugir de mim, mas onde eu ia eu tava.' +'&quot;',
		source: 'Francisco Everaldo de Oliveira',
	},
];

let beforeQuote = null;

function sortNumber(maxNumber, beforeNumber){
	let randomNumber = Math.floor(Math.random() * maxNumber)

	if(beforeNumber !== null) {
		while (randomNumber === beforeNumber) {
			randomNumber = Math.floor(Math.random() * maxNumber);
		}

		beforeNumber = randomNumber;
	}
	
	return randomNumber;
}

let secunds = 0;

function changeQuote(quotes){

}

function rendeElements(){
	const elementRoot = document.getElementById('root');
	const quote = quotes[sortNumber(quotes.length, beforeQuote)];
	const body = document.querySelector('body')

	body.style.backgroundColor = `rgb(${sortNumber(256)},${sortNumber(256)},${sortNumber(256)})`;

	elementRoot.innerHTML = 
		`<blockquote>
			${quote.text} : ${secunds}
			<cite> - ${quote.source}</cite>
		</blockquote>`;
}


rendeElements()